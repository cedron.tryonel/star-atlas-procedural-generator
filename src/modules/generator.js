"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParseSeed = exports.generateGalaxy = exports.getCenter = exports.getDistance = exports.isSystem = exports.seedCoords = exports.parseCoords = void 0;
var js_sha256_1 = require("js-sha256");
var utf8_1 = require("utf8");
function parseCoords(coords) {
    coords = coords.replace(/[() ]/g, "");
    var coordsSplit = coords.split(",");
    if (coordsSplit[1]) {
        return coordsSplit;
    }
    else {
        return false;
    }
}
exports.parseCoords = parseCoords;
function seedCoords(x, y) {
    var seedStr = "(" + x + "," + y + ")";
    var seed = js_sha256_1.sha256(utf8_1.encode(seedStr));
    return seed;
}
exports.seedCoords = seedCoords;
function isSystem(seed) {
    if (seed[0] + seed[1] == "ff") {
        return true;
    }
    else {
        return false;
    }
}
exports.isSystem = isSystem;
function getDistance(x1, y1, x2, y2) {
    var pythagorean = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    return pythagorean.toFixed(2).toLocaleString();
}
exports.getDistance = getDistance;
function getCenter(min, max) {
    var center = Math.round((min + max) / 2);
    return center;
}
exports.getCenter = getCenter;
function generateGalaxy(min, max, shape) {
    var countAll = 0;
    var countSystems = 0;
    var firstSystem = "";
    var center = getCenter(min, max);
    var radius = max - center;
    var width = max - min;
    var map = "";
    for (var x = min; x <= max; x++) { // iterate through entire x axis
        for (var y = min; y <= max; y++) { // iterate through entire y axis
            // if galaxy is circle, check distance to apply radius filter, else just check everything
            if (shape == "circle") {
                // Get distance between current coordinates and center coordinates and match against radius length
                if (parseInt(getDistance(x, y, center, center)) <= radius) {
                    countAll += 1;
                }
            }
            else if (shape == "square") { // process galaxy as a square
                countAll += 1;
            }
            if ((parseInt(getDistance(x, y, center, center)) <= radius && shape == "circle") || shape == "square") {
                // generate seed and check if is star system
                var seed = seedCoords(x, y);
                if (seed[0] + seed[1] == "ff") {
                    countSystems += 1;
                    map += "<img src=\"images/dot.png\" style=\"position: absolute; left: " + (x + Math.abs(min)) + "px; top: " + (y + Math.abs(min)) + "px;\">";
                    if (firstSystem == "") {
                        firstSystem = "(" + x + ", " + y + ")";
                    }
                }
            }
        }
    }
    return [countAll, countSystems, firstSystem, width, map];
}
exports.generateGalaxy = generateGalaxy;
var ParseSeed = /** @class */ (function () {
    function ParseSeed(seed) {
        this.seed = seed;
        this.starType = this.getStarType(this.seed);
        this.starQuantity = this.getStarQuantity(this.seed);
        this.planetQuantity = this.getPlanetQuantity(this.seed).toString();
    }
    ParseSeed.prototype.getStarType = function (seed) {
        var char = seed[2];
        var starType = {
            "0": "Blue Giant",
            "1": "Blue",
            "2": "Blue Dwarf",
            "3": "White Giant",
            "4": "White",
            "5": "White Dwarf",
            "6": "Yellow Giant",
            "7": "Yellow",
            "8": "Yellow",
            "9": "Yellow Dwarf",
            "a": "Orange Giant",
            "b": "Orange",
            "c": "Orange Dwarf",
            "d": "Red Giant",
            "e": "Red",
            "f": "Red Dwarf"
        };
        return starType[char];
    };
    ParseSeed.prototype.getStarQuantity = function (seed) {
        var char = seed[3];
        if (char == "f") {
            return "Binary";
        }
        else {
            return "Unary";
        }
    };
    ParseSeed.prototype.getPlanetQuantity = function (seed) {
        var seedStr = seed[4];
        var planet = 0;
        if (seedStr == "0") {
            planet = 1;
        }
        else if (seedStr.search(/[12]/)) {
            planet = 2;
        }
        else if (seedStr.search(/[3-5]/)) {
            planet = 3;
        }
        else if (seedStr.search(/[6-9]/)) {
            planet = 4;
        }
        else if (seedStr.search(/[a-c]/)) {
            planet = 5;
        }
        else if (seedStr.search(/[de]/)) {
            planet = 6;
        }
        else if (seedStr == "f") {
            planet = 7;
        }
        return planet;
    };
    return ParseSeed;
}());
exports.ParseSeed = ParseSeed;
//# sourceMappingURL=generator.js.map