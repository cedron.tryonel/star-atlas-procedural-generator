import { sha256 } from 'js-sha256'
import { encode } from 'utf8'

export function parseCoords(coords: string) {
    coords = coords.replace(/[() ]/g, "")
    let coordsSplit = coords.split(",")
    if (coordsSplit[1]) {
        return coordsSplit
    } else {
        return false
    }
}

export function seedCoords(x: number, y: number): string {
    let seedStr = "(" + x + "," + y + ")"
    let seed = sha256(encode(seedStr))
    return seed
}

export function isSystem(seed: string): boolean {
    if (seed[0] + seed[1] == "ff") {
        return true
    } else {
        return false
    }
}

export function getDistance(x1: number, y1: number, x2: number, y2: number) {
    let pythagorean = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2))
    return pythagorean.toFixed(2).toLocaleString()
}

export function getCenter(min: number, max: number) {
    let center = Math.round((min + max) / 2)
    return center
}

export function generateGalaxy(min: number, max: number, shape: string) {
    let countAll = 0
    let countSystems = 0
    let firstSystem = ""
    let center = getCenter(min, max)
    let radius = max - center
    let width = max - min
    let map = ""

    for (let x = min; x <= max; x++) { // iterate through entire x axis
        for (let y = min; y <= max; y++) { // iterate through entire y axis
            // if galaxy is circle, check distance to apply radius filter, else just check everything
            if (shape == "circle") {
                // Get distance between current coordinates and center coordinates and match against radius length
                if (parseInt(getDistance(x, y, center, center)) <= radius) {
                    countAll += 1
                }
            } else if (shape == "square") { // process galaxy as a square
                countAll += 1
            }

            if ((parseInt(getDistance(x, y, center, center)) <= radius && shape == "circle") || shape == "square") {
                // generate seed and check if is star system
                let seed = seedCoords(x, y)
                if (seed[0] + seed[1] == "ff") {
                    countSystems += 1

                    map += "<img src=\"images/dot.png\" style=\"position: absolute; left: " + (x + Math.abs(min)) + "px; top: " + (y + Math.abs(min)) + "px;\">"
                    
                    if (firstSystem == "") {
                        firstSystem = "(" + x + ", " + y + ")"
                    }
                }
            }
        }
    }
    return [countAll, countSystems, firstSystem, width, map]
}

export class ParseSeed {
    seed: string
    starType: string
    starQuantity: string
    planetQuantity: string

    constructor(seed: string) {
        this.seed = seed
        this.starType = this.getStarType(this.seed)
        this.starQuantity = this.getStarQuantity(this.seed)
        this.planetQuantity = this.getPlanetQuantity(this.seed).toString()
    }

    getStarType(seed: string): string {
        let char = seed[2]
        const starType = {
            "0": "Blue Giant",
            "1": "Blue",
            "2": "Blue Dwarf",
            "3": "White Giant",
            "4": "White",
            "5": "White Dwarf",
            "6": "Yellow Giant",
            "7": "Yellow",
            "8": "Yellow",
            "9": "Yellow Dwarf",
            "a": "Orange Giant",
            "b": "Orange",
            "c": "Orange Dwarf",
            "d": "Red Giant",
            "e": "Red",
            "f": "Red Dwarf"
        }
        return (starType as any)[char]
    }

    getStarQuantity(seed: string): string {
        let char = seed[3]

        if (char == "f") {
            return "Binary"
        } else {
            return "Unary"
        }
    }

    getPlanetQuantity(seed: string): number {
        let seedStr = seed[4]
        let planet = 0

        if (seedStr == "0") {
            planet = 1
        } else if (seedStr.search(/[12]/)) {
            planet = 2
        } else if (seedStr.search(/[3-5]/)) {
            planet = 3
        } else if (seedStr.search(/[6-9]/)) {
            planet = 4
        } else if (seedStr.search(/[a-c]/)) {
            planet = 5
        } else if (seedStr.search(/[de]/)) {
            planet = 6
        } else if (seedStr == "f") {
            planet = 7
        }

        return planet
    }
}

