import logo from './logo.svg';
import './App.css';
import GenerateGalaxy from './components/GenerateGalaxy';
import CheckCoords from './components/CheckCoords';

function App() {
  return (
      <div style={{backgroundColor: "#444", color: "white"}}>
          <GenerateGalaxy />
          <CheckCoords />
       </div>
  );
}

export default App;
