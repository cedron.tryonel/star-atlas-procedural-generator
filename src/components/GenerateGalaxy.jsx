import React, { Component } from 'react'
import { generateGalaxy, getDistance, getCenter } from '../modules/generator'

class GenerateGalaxy extends Component {
    state = {
        galaxyShape: "",
        totalLocations: 0,
        starSystems: 0,
        firstSystem: "",
        maxDistance: "",
        min: 0,
        max: 0,
        galaxyCenter: "",
        galaxyWidthPx: "600px",
    }

    process = () => {
        let minVal = parseInt(document.getElementById("coordsMin").value)
        let maxVal = parseInt(document.getElementById("coordsMax").value)
        let shape = document.getElementById("galShape").value
        this.setState({ min: minVal })
        this.setState({ max: maxVal })
        this.setState({ galaxyShape: shape })
        let center = getCenter(minVal, maxVal)
        this.setState({ galaxyCenter: "(" + center + ", " + center + ")" })

        // set maximum distance from edge to edge
        if (shape == "circle") {
            // distance is a radius * 2
            this.setState({ maxDistance: "Distance of " + (getDistance(center, center, center, maxVal) * 2) + " units in diameter" })
        } else if (shape = "square") {
            // distance is corner to corner
            this.setState({ maxDistance: "Distance of " + getDistance(minVal, minVal, maxVal, maxVal) + " units from corner to corner" })
        }
        
        let galaxyData = generateGalaxy(minVal, maxVal, shape)
        this.setState({ totalLocations: galaxyData[0] })
        this.setState({ starSystems: galaxyData[1] })
        this.setState({ firstSystem: galaxyData[2] })
        this.setState({ galaxyWidthPx: galaxyData[3] })
        this.setState({ galMap: galaxyData[4] })
        document.getElementById("galMap").innerHTML = galaxyData[4]
    }

    render() { 
        return (
            <React.Fragment>
                <div style={{padding: "20px", border: "2px solid black"}}>
                    <center><h2>Generate Galaxy</h2></center>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="card">
                                    <div className="card-header bg-secondary">
                                        Input Data
                                    </div>
                                    <div className="card-body bg-secondary">
                                        Coordinates Min: <input style={{ width: "100%" }} type="number" id="coordsMin"></input>
                                        Coordinates Max: <input style={{ width: "100%" }} type="number" id="coordsMax"></input>
                                        Galaxy Shape: <select style={{ width: "100%" }} id="galShape">
                                            <option value="square">Square</option>
                                            <option value="circle">Circle</option>
                                        </select>
                                        <center><button className="btn btn-primary" style={{ margin: "10px" }} onClick={this.process}>Generate!</button></center>
                                    </div>
                                </div>
                            </div>

                            <div className="col-8">
                                <div className="card">
                                    <div className="card-header bg-secondary">
                                        Output Data
                                    </div>
                                    <div className="card-body bg-secondary">
                                        <table cellPadding="5">
                                            <tbody>
                                                <tr>
                                                    <td>Galaxy Shape:</td><td>{ this.state.galaxyShape }</td>
                                                </tr>
                                                <tr>
                                                    <td>Galaxy Center:</td><td>{this.state.galaxyCenter}</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Locations:</td><td>{ this.state.totalLocations }</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Star Systems:</td><td>{ this.state.starSystems }</td>
                                                </tr>
                                                <tr>
                                                    <td>Maximum Distance:</td>
                                                    <td>{ this.state.maxDistance }</td>
                                                </tr>
                                                <tr>
                                                    <td>First System:</td><td>{ this.state.firstSystem }</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="card">
                                    <div className="card-header bg-secondary">
                                        Galaxy Map
                                    </div>
                                    <div className="card-body bg-secondary">
                                        <div id="galMap" style={{ margin: "auto", position: "relative", backgroundColor: "black", maxWidth: "100%", maxHeight: "100%", width: this.state.galaxyWidthPx, height: this.state.galaxyWidthPx }}>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
 
export default GenerateGalaxy;