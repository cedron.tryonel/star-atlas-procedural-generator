import React, { Component } from 'react';
import { parseCoords, seedCoords, isSystem, ParseSeed } from '../modules/generator';

class CheckCoords extends Component {
    state = {
        coords: "",
        seed: "",
        isSystem: "",
        starType: "",
        starQuantity: "",
        planetQuantity: "",
    }

    process = (event) => {
        let coords = parseCoords(event.target.value)
        let x = coords[0]
        let y = coords[1]
        if (Number.isInteger(parseInt(y))) {
            this.setState({ coords: "(" + x + ", " + y + ")" })
            let seed = seedCoords(x, y)
            this.setState({ seed: seed })
            if (isSystem(seed)) {
                this.setState({ isSystem: "Yes" })
                let data = new ParseSeed(seed)
                this.setState({ starType: data.starType })
                this.setState({ starQuantity: data.starQuantity })
                this.setState({ planetQuantity: data.planetQuantity })
            } else {
                this.setState({ isSystem: "No" })
                this.setState({ starType: "" })
                this.setState({ starQuantity: "" })
                this.setState({ planetQuantity: "" })
            }
        }
    };

    render() {
        return (
            <React.Fragment>
                <div style={{padding: "20px", border: "2px solid black"}}>
                    <center><h2>Check Coordinates</h2></center>
                    <div className="container">
                        <div className="row">
                            <div className="col-4">
                                <div className="card">
                                    <div className="card-header bg-secondary">
                                        Input Data
                                    </div>
                                    <div className="card-body bg-secondary">
                                        Coordinates: <input onChange={this.process} style={{ width: "100%" }} type="text" name="coordinates" placeholder="(x, y)"></input>
                                    </div>
                                </div>
                            </div>

                            <div className="col-8">
                                <div className="card">
                                    <div className="card-header bg-secondary">
                                        Output Data
                                    </div>
                                    <div className="card-body bg-secondary">
                                        <table cellPadding="5">
                                            <tbody>
                                                <tr>
                                                    <td>Coords:</td><td>{this.state.coords}</td>
                                                </tr>
                                                <tr>
                                                    <td>Seed:</td><td>{this.state.seed}</td>
                                                </tr>
                                                <tr>
                                                    <td>Is System:</td><td>{this.state.isSystem}</td>
                                                </tr>
                                                <tr>
                                                    <td>Star Type:</td><td>{this.state.starType}</td>
                                                </tr>
                                                <tr>
                                                    <td>Star Quantity:</td><td>{this.state.starQuantity}</td>
                                                </tr>
                                                <tr>
                                                    <td>Planet Quantity:</td><td>{this.state.planetQuantity}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default CheckCoords;